create database aulas;
use aulas;
CREATE TABLE cliente (
  idcliente INT NOT NULL AUTO_INCREMENT,
  nome VARCHAR(45) NULL,
  cpf VARCHAR(15) NULL,
  telefone VARCHAR(15) NULL,
  endereco VARCHAR(45) NULL,
  email VARCHAR(45) NULL,
  datanasc DATE NULL,
  PRIMARY KEY (`idcliente`));

CREATE DATABASE ProjetoJava;

USE ProjetoJava;

CREATE TABLE funcionario (idfuncionario INT (2),
						  Nome VARCHAR(50),
						  CPF VARCHAR(20),
                          Telefone VARCHAR(20),
                          Endereco VARCHAR(50),
                          Email VARCHAR(100),
                          DataNasc DATE,
                          Cargo VARCHAR(20),
                          Salario DECIMAL(8,2),
                          Turno VARCHAR(20),
CONSTRAINT Pk_funcionario PRIMARY KEY (idfuncionario)
);
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dados;

import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class ClienteDAO extends DAO<Cliente> {

    @Override
    public boolean insert(Cliente element) {
        try {
            String sql = "INSERT INTO cliente (nome,cpf,telefone,endereco,email,datanasc) VALUES (?,?,?,?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            stmt.setString(1, element.getNome());
            stmt.setString(2, element.getCpf());
            stmt.setString(3, element.getTelefone());
            stmt.setString(4, element.getEndereco());
            stmt.setString(5, element.getEmail());
            stmt.setString(6, element.getEscolaridade());
            stmt.setInt   (7, element.getIdCliente());

            System.out.println("SQL: " + sql);
            int linhas = stmt.executeUpdate();
            if (linhas > 0) 
            {
                ResultSet rs = stmt.getGeneratedKeys();
                if (rs.next()) {
                    int key = rs.getInt(1);
                    element.setIdCliente(key);
                } else {
                    System.out.printf("Erro Inesperado\n");
                    return false;
                }
            }
           return true;

        } catch (SQLException e) 
        {
            System.err.printf("Erro ao Executar INSERT: %s\n", e.getMessage());
        }
        return false;
    }

    @Override
    public boolean update(Cliente element) {
        try
        {
            String sql = "UPDATE cliente SET nome=?,cpf=?,telefone=?,endereco=?,email=?,datanasc=? WHERE idcliente=?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1,element.getNome());
            stmt.setString(2,element.getCpf());
            stmt.setString(3,element.getTelefone());
            stmt.setString(4,element.getEndereco());
            stmt.setString(5,element.getEmail());
            stmt.setString(6,element.getEscolaridade());
            stmt.setInt   (7,element.getIdCliente());  
            
            System.out.println("SQL: "+sql);
            int linhas = stmt.executeUpdate();
            if(linhas>0)
            {
                ResultSet rs = stmt.getGeneratedKeys();
                if(rs.next())
                {
                    int key = rs.getInt(1);
                    element.setIdCliente(key);
                }
                else
                {
                    System.out.printf("erro inesperado\n");
                    return false;
                }
            }return true;
        }
        catch(SQLException e)
        {
            System.err.printf("Erro ao executar UPDATE: %s\n", e.getMessage());
        }
        return false;
    }

    @Override
    public boolean delete(Cliente element) {
        try
        {
            String sql = "DELETE FROM cliente WHERE idcliente =?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1,element.getIdCliente());
            if(stmt.executeUpdate()>0) return true;
        }
        catch(SQLException e)
        {
            System.out.printf("Erro ao EXCLUIR: %s\n", e.getMessage());
        }
        return false;
    }

    @Override
    public List<Cliente> list() {
        List<Cliente> lst = new LinkedList<>();
        lst = org.jdesktop.observablecollections.ObservableCollections.observableList(lst);
        
        try
        {
            String sql = "SELECT * FROM cliente";
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while(rs.next())
            {
                Cliente cl = new Cliente();
                cl.setIdCliente(rs.getInt("idcliente"));
                cl.setNome(rs.getString("nome"));
                cl.setCpf(rs.getString("cpf"));
                cl.setTelefone(rs.getString("telefone"));
                cl.setEndereco(rs.getString("endereco"));
                cl.setEmail(rs.getString("email"));
                cl.setEscolaridade(rs.getString("escolaridade"));
            }
        }
        catch(SQLException e)
        {
            System.out.printf("Erro ao Listar: %s\n", e.getMessage());
        }
        return lst;
    }
    public void list (String buscar, List<Cliente> lst)
    {
        lst.clear();
        try
        {
            String sql = "SELECT * FROM cliente WHERE nome LIKE ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1,"%"+buscar+"%");
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                Cliente cl = new Cliente();
                cl.setIdCliente(rs.getInt("idcliente"));
                cl.setNome(rs.getString("nome"));
                cl.setCpf(rs.getString("cpf"));
                cl.setTelefone(rs.getString("telefone"));
                cl.setEndereco(rs.getString("endereco"));
                cl.setEmail(rs.getString("email"));
                cl.setEscolaridade(rs.getString("escolaridade"));
                lst.add(cl);
            }
        }
        catch(SQLException e)
        {
            System.out.printf("Erro ao Listar: %s\n", e.getMessage());
        }
    }
}

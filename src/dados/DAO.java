/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dados;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
/**
 *
 * @author Lab04
 */
public abstract class DAO <L> {
    
    protected Connection conn;
    
    public DAO (){
        
        try
        {
            String url="jdbc:mysql://localhost/projetojava";
            String user="root";
            String pass="root";
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url,user,pass);
        }
        catch (ClassNotFoundException e)
        {
            e.printStackTrace();
            System.err.printf("Erro ao Carregar Driver");
        }
        catch(SQLException e)
        {
            e.printStackTrace();
            System.err.printf("Erro ao Conectar: %s", e.getMessage());
        }
    }
    public abstract boolean insert(L element);
    public abstract boolean update(L element);
    public abstract boolean delete(L element);
    public abstract List<L> list();
}

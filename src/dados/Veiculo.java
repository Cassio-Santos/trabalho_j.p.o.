/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dados;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

/**
 *
 * @author User
 */
public class Veiculo {
    
    private Integer idVeiculo;

    public static final String PROP_IDVEICULO = "idVeiculo";

    public Integer getIdVeiculo() {
        return idVeiculo;
    }

    public void setIdVeiculo(Integer idVeiculo) {
        Integer oldIdVeiculo = this.idVeiculo;
        this.idVeiculo = idVeiculo;
        propertyChangeSupport.firePropertyChange(PROP_IDVEICULO, oldIdVeiculo, idVeiculo);
    }
    
    private String marca;

    public static final String PROP_MARCA = "marca";

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        String oldMarca = this.marca;
        this.marca = marca;
        propertyChangeSupport.firePropertyChange(PROP_MARCA, oldMarca, marca);
    }

    private transient final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }

    private String ano;

    public static final String PROP_ANO = "ano";

    public String getAno() {
        return ano;
    }

    public void setAno(String ano) {
        String oldAno = this.ano;
        this.ano = ano;
        propertyChangeSupport.firePropertyChange(PROP_ANO, oldAno, ano);
    }

    private String nome;

    public static final String PROP_NOME = "nome";

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        String oldNome = this.nome;
        this.nome = nome;
        propertyChangeSupport.firePropertyChange(PROP_NOME, oldNome, nome);
    }

    private String modelo;

    public static final String PROP_MODELO = "modelo";

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        String oldModelo = this.modelo;
        this.modelo = modelo;
        propertyChangeSupport.firePropertyChange(PROP_MODELO, oldModelo, modelo);
    }

    private String categoria;

    public static final String PROP_CATEGORIA = "categoria";

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        String oldCategoria = this.categoria;
        this.categoria = categoria;
        propertyChangeSupport.firePropertyChange(PROP_CATEGORIA, oldCategoria, categoria);
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dados;

import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Cassio
 */
public class FuncionarioDAO extends DAO<Funcionario> {

@Override
public boolean insert (Funcionario element){
  try
    {
        String sql = "INSERT INTO funcionario (Nome,CPF,Telefone,Endereco,Email,Cargo,Salario,Turno,DataNasc)"
                + " VALUES (?,?,?,?,?,?,?,?,?)";
        PreparedStatement stmt = conn.prepareStatement (sql,
                                    Statement.RETURN_GENERATED_KEYS);
        stmt.setString(1, element.getNome());
        stmt.setString(1, element.getCPF());
        stmt.setString(1, element.getTelefone());
        stmt.setString(1, element.getEndereco());
        stmt.setString(1, element.getEmail());
        stmt.setString(1, element.getCargo());
        stmt.setString(1, element.getSalario());
        if(element.getDataNasc() != null){
            java.sql.Date auxDate =
                    new java.sql.Date(element.getDataNasc().getTime());
            stmt.setDate(6, auxDate);
        }else
            stmt.setDate(6,null);
                
        System.out.println("SQL: "+sql);
        int linhas = stmt.executeUpdate();
        if(linhas>0){
            ResultSet rs = stmt.getGeneratedKeys();
            if(rs.next()){
                int key = rs.getInt(1);
                element.setIdfuncionario(key);
            }
            else{
                    System.out.printf("erro inesperado\n");
                    return false;
                }
                return true;
            }
        }
        catch(SQLException e){
            System.err.printf("erro ao executar insert: %s\n", e. getMessage());
        }
        return false;
    }

    @Override
    public boolean update(Funcionario element) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean delete(Funcionario element) {
        try
        {
            String sql = "DELETE FROM Funcionario WHERE idfuncionario = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, element.getIdfuncionario());
            if(stmt.executeUpdate()>0)
                return true;
        }catch(SQLException e)
        {
            System.out.printf("Errro ao excluir: %s\n",e.getMessage());
        }
         return false;
    }

    @Override
    public List<Funcionario> list() {
        List<Funcionario> lst = new LinkedList<>();
        lst = org.jdesktop.observablecollections.ObservableCollections.
                                                               observableList(lst);
        
        try{
            String sql = "SELECT * Funcionario";
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            
            while(rs.next())
            {
                Funcionario fu = new Funcionario();
                    
                fu.setIdfuncionario(rs.getInt("idfuncionario"));
                fu.setNome(rs.getString("Nome"));
                fu.setCPF(rs.getString("CPF"));
                fu.setTelefone(rs.getString("Telefone"));
                fu.setEndereco(rs.getString("Endereco"));
                fu.setEmail(rs.getString("Email"));
                fu.setDataNasc(rs.getDate("DataNasc"));
                fu.setCargo(rs.getString("Cargo"));
                fu.setSalario(rs.getString("Salario"));
                fu.setTurno(rs.getString("Turno"));
                
                lst.add(fu);
            }
        }
        catch(SQLException e)
        {
            System.out.printf("erro ao listar: %s\n",e.getMessage());
        }
        return lst;
    }

}

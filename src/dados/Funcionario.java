/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dados;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Date;


/**
 *
 * @author Cassio Santos
 */
public class Funcionario {
    
    private String Nome;

    public static final String PROP_NOME = "Nome";

    public String getNome() {
        return Nome;
    }

    public void setNome(String Nome) {
        String oldNome = this.Nome;
        this.Nome = Nome;
        propertyChangeSupport.firePropertyChange(PROP_NOME, oldNome, Nome);
    }

    private transient final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }

    private String CPF;

    public static final String PROP_CPF = "CPF";

    public String getCPF() {
        return CPF;
    }

    public void setCPF(String CPF) {
        String oldCPF = this.CPF;
        this.CPF = CPF;
        propertyChangeSupport.firePropertyChange(PROP_CPF, oldCPF, CPF);
    }

    private String Telefone;

    public static final String PROP_TELEFONE = "Telefone";

    public String getTelefone() {
        return Telefone;
    }

    public void setTelefone(String Telefone) {
        String oldTelefone = this.Telefone;
        this.Telefone = Telefone;
        propertyChangeSupport.firePropertyChange(PROP_TELEFONE, oldTelefone, Telefone);
    }

    private String Endereco;

    public static final String PROP_ENDERECO = "Endereco";

    public String getEndereco() {
        return Endereco;
    }

    public void setEndereco(String Endereco) {
        String oldEndereco = this.Endereco;
        this.Endereco = Endereco;
        propertyChangeSupport.firePropertyChange(PROP_ENDERECO, oldEndereco, Endereco);
    }

    private String Email;

    public static final String PROP_EMAIL = "Email";

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        String oldEmail = this.Email;
        this.Email = Email;
        propertyChangeSupport.firePropertyChange(PROP_EMAIL, oldEmail, Email);
    }

       private Date DataNasc;

    public static final String PROP_DATANASC = "DataNasc";

    public Date getDataNasc() {
        return DataNasc;
    }

    public void setDataNasc(Date DataNasc) {
        Date oldDataNasc = this.DataNasc;
        this.DataNasc = DataNasc;
        propertyChangeSupport.firePropertyChange(PROP_DATANASC, oldDataNasc, DataNasc);
    }


    private String Cargo;

    public static final String PROP_CARGO = "Cargo";

    public String getCargo() {
        return Cargo;
    }

    public void setCargo(String Cargo) {
        String oldCargo = this.Cargo;
        this.Cargo = Cargo;
        propertyChangeSupport.firePropertyChange(PROP_CARGO, oldCargo, Cargo);
    }

    private String Salario;

    public static final String PROP_SALARIO = "Salario";

    public String getSalario() {
        return Salario;
    }

    public void setSalario(String Salario) {
        String oldSalario = this.Salario;
        this.Salario = Salario;
        propertyChangeSupport.firePropertyChange(PROP_SALARIO, oldSalario, Salario);
    }

    private String Turno;

    public static final String PROP_TURNO = "Turno";

    public String getTurno() {
        return Turno;
    }

    public void setTurno(String Turno) {
        String oldTurno = this.Turno;
        this.Turno = Turno;
        propertyChangeSupport.firePropertyChange(PROP_TURNO, oldTurno, Turno);
    }

    private Integer idfuncionario;

    public static final String PROP_IDFUNCIONARIO = "idfuncionario";

    public Integer getIdfuncionario() {
        return idfuncionario;
    }

    public void setIdfuncionario(Integer idfuncionario) {
        Integer oldIdfuncionario = this.idfuncionario;
        this.idfuncionario = idfuncionario;
        propertyChangeSupport.firePropertyChange(PROP_IDFUNCIONARIO, oldIdfuncionario, idfuncionario);
    }

}

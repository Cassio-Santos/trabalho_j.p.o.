/* teste 15:54
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dados;

import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author User
 */
public class VeiculoDAO extends DAO<Veiculo>{
    @Override
    public boolean insert(Veiculo element) {
        try
        {
           
            String sql = "INSERT INTO veiculo (marca,ano,nome,"
                    + "modelo,categoria) values (?,?,?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql,
                                        Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, element.getMarca());
            stmt.setString(2, element.getAno());
            stmt.setString(3, element.getNome());
            stmt.setString(4, element.getModelo());
            stmt.setString(5, element.getCategoria());
            
                
            System.out.println("SQL: "+sql);
            int linhas = stmt.executeUpdate();
            if(linhas>0)
            {
                ResultSet rs = stmt.getGeneratedKeys();
                if(rs.next())
                {
                    int key = rs.getInt(1);
                    element.setIdVeiculo(key);
                }
                else
                {
                    System.out.printf("erro inesperado\n");
                    return false;
                }
                return true;
            }
        }
        catch(SQLException e)
        {
            System.err.printf("erro ao executar insert: %s\n",e.getMessage());
        }
        return false;
    }
    public boolean delete(Veiculo element) {
        try
        {
            String sql = "DELETE FROM veiculo WHERE idveiculo = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, element.getIdVeiculo());
            if(stmt.executeUpdate()>0)
                return true;
        }catch(SQLException e)
        {
            System.out.printf("Errro ao excluir: %s\n",e.getMessage());
        }
        
        return false;
    }
    
    public boolean update(Veiculo element) {
        try
        {
            String sql = "UPDATE cliente SET nome=?, cpf = ?, telefone=?, "
                    + "endereco=?, email=?, datanasc=? where idcliente=?; ";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, element.getMarca());
            stmt.setString(2, element.getAno());
            stmt.setString(3, element.getNome());
            stmt.setString(4, element.getModelo());
            stmt.setString(5, element.getCategoria());
            
            stmt.setInt(6, element.getIdVeiculo());
            if(stmt.executeUpdate()>0)
            {
                return true;
            }
        }
        catch(SQLException e)
        {
            System.out.printf("Erro ao atualizar: %s\n",e.getMessage());
        }
        return false;
    }

    

    @Override
    public List<Veiculo> list() {
        List<Veiculo> lst = new LinkedList<>();
        lst = org.jdesktop.observablecollections.ObservableCollections.
                                                            observableList(lst);
        
        try
        {
            String sql = "SELECT * FROM veiculo";
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while(rs.next())
            {
                Veiculo cl = new Veiculo();
                cl.setIdVeiculo(rs.getInt("idveiculo"));
                cl.setMarca(rs.getString("Marca"));
                cl.setAno(rs.getString("Ano"));
                cl.setNome(rs.getString("Nome"));
                cl.setModelo(rs.getString("Modelo"));
                cl.setCategoria(rs.getString("Categoria"));
                
                lst.add(cl);
            }
        }
        catch(SQLException e)
        {
            System.out.printf("erro ao listar: %s\n",e.getMessage());
        }
        return lst;
    }
    
    public void list(String buscar, List<Veiculo> lst) {
        lst.clear();
        try
        {
            String sql = "SELECT * FROM veiculo WHERE nome LIKE ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, "%"+buscar+"%");
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                Veiculo cl = new Veiculo();
                cl.setIdVeiculo(rs.getInt("idveiculo"));
                cl.setMarca(rs.getString("Marca"));
                cl.setAno(rs.getString("Ano"));
                cl.setNome(rs.getString("Nome"));
                cl.setModelo(rs.getString("Modelo"));
                cl.setCategoria(rs.getString("Categoria"));
                
                lst.add(cl);
            }
        }
        catch(SQLException e)
        {
            System.out.printf("erro ao listar: %s\n",e.getMessage());
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package convertData;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Cassio Santos
 */
public class ConversorTextoParaData extends org.jdesktop.beansbinding.Converter<String,Date>{

    private SimpleDateFormat d = new SimpleDateFormat("dd/MM/yyyy");
    
    @Override
    public Date convertForward(String value) {
        try{
            return d.parse(value);
        }
        catch(ParseException e){
            System.out.printf("Data inválida\n");
            return null;
        }
    }

    @Override
    public String convertReverse(Date value) {
        return d.format(value);
    }
    
    
}

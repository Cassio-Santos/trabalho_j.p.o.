/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package convertData;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;
/**
 *
 * @author Cassio Santos
 */
public class ConversorDataParatexto extends org.jdesktop.beansbinding.Converter<Date,String>{
    
    private SimpleDateFormat d;
    
    public ConversorDataParatexto(){
        d = new SimpleDateFormat("dd/MM/yyyy");
    }

    @Override
    public String convertForward(Date value) {
        return d.format(value);
    }

    @Override
    public Date convertReverse(String value) {
        try{
            return d.parse(value);
        }catch(ParseException e){
            JOptionPane.showMessageDialog(null,"Formato de data inválido");
            return null;
        }
    }
    
}

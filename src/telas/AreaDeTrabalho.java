/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package telas;

import javax.swing.JDesktopPane;

/**
 *
 * @author User
 */
public class AreaDeTrabalho extends JDesktopPane {
    
    private CadastroCliente novoCliente;
    private CadastroVeiculo novoVeiculo;
    private CadastroFuncionario novoFuncionario;
    
    void iniciarCadastroCliente()
    {
        if(novoCliente == null)
        {
            novoCliente = new CadastroCliente();
            add(novoCliente);
            novoCliente.setVisible(true);
        }
       
    }
    
    
   void iniciarCadastroVeiculo()
    {
        if(novoVeiculo == null)
        {
            novoVeiculo = new CadastroVeiculo();
            add(novoVeiculo);
            novoVeiculo.setVisible(true);
        }
        
    }
    
    void iniciarCadastroFuncinario()
    {
        if(novoFuncionario == null)
        {
            novoFuncionario = new CadastroFuncionario();
            add(novoFuncionario);
            novoFuncionario.setVisible(true);
        }
        
    }
    
    void fecharCadastroCliente()
    {
        novoCliente = null;
    }
    
    void fecharCadastroVeiculo()
    {
        novoVeiculo = null;
    }
    
    void fecharCadastroFuncionario()
    {
        novoFuncionario = null;
    }
    
}
